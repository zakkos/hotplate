/***********************************
 * File:    shift_74ls164.h
 * Author:  zakkos
 ***********************************/

#ifndef SHIFT_74LS164_H
#define	SHIFT_74LS164_H

#ifdef	__cplusplus
extern "C" {
#endif

//if defined inverts the output bits (IE 1->0 and 0->1). Useful for open drain outputs that require pullups
//#define SHIFT_INVERSE

//################### DEFINITIONS ###################

//output ports
#define SHIFT_OUT PORTBbits.RB4
#define SHIFT_CLK PORTBbits.RB5

//################### FUNCTION PROTOTYPES ###################

void _shiftOut(volatile unsigned char value);

#ifdef	__cplusplus
}
#endif

#endif	/* SHIFT_74LS164_H */

