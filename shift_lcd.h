/***********************************
 * File:    shift_lcd.h
 * Author:  zakkos
 ***********************************/

#ifndef SHIFT_LCD

#define SHIFT_LCD

#ifndef _XTAL_FREQ
#define _XTAL_FREQ      4000000
#endif

//#define SHIFT_REGISTER_595            // Select which type of shift register is in use

#ifdef SHIFT_REGISTER_595
    #ifndef SHIFT_595_H
        #include "shift_595.c"
    #endif
#else
    #ifndef SHIFT_74LS164_H
        #include "shift_74ls164.c"
    #endif
#endif

//################### DEFINITIONS ###################

// Pin selection
#define DispRS          PORTAbits.RA3
#define DispE           PORTAbits.RA4

// Register selection
#define CMD_REG         DispRS = 0    // Command register
#define DATA_REG        DispRS = 1    // Data register

// LCD specific defines
#define LCD_CLR         0x01    // Clears display
#define LCD_HOME        0x02    // Returns home

// Entry mode set
#define LCD_DEC         0x04    // Decrements DDRAM address
#define LCD_SHIFT_R     0x05    // Shifts display to the right
#define LCD_INC         0x06    // Increments DDRAM address cursor from left to right
#define LCD_SHIFT_L     0x07    // Shifts display to the left

// Display ON/OFF
#define LCD_DISPOFF     0x08    // Display off
#define LCD_DISPON      0x0C    // Display on
#define LCD_CRS_OFF     LCD_DISPON | 0x00    // Cursor does not display
#define LCD_CRS_ON      LCD_DISPON | 0x02    // Cursor displays
#define LCD_BL_OFF      LCD_DISPON | 0x00    // Cursor does not blink
#define LCD_BL_ON       LCD_CRS_ON | 0x01    // Cursor blinks

// Cursor and Display Shift
#define LCD_SH_C_L      0x10    // Shifts the cursor position to the left (Address Counter is decremented by 1)
#define LCD_SH_C_R      0x14    // Shifts the cursor position to the right (Address Counter is incremented by 1)
#define LCD_SH_D_L      0x18    // Shifts the entire display to the left. The cursor follows the display shift
#define LCD_SH_D_R      0x1C    // Shifts the entire display to the right. The cursor follows the display shift

// Function Set
//8bit
#define LCD_8b_2L      0x38    // Function set. Data 8-bit. Displays 2 line. Character font: 5x 8 dots
#define LCD_8b_1L      0x30    // Function set. Data 8-bit. Displays 1 line. Character font: 5x 8 dots
// 4bit
#define LCD_4b_2L      0x28    // Function set. Data 4-bit. Displays 2 line. Character font: 5x 8 dots
#define LCD_4b_1L      0x20    // Function set. Data 4-bit. Displays 1 line. Character font: 5x 8 dots

// LCD defines for calling lcd_xy with line number, position and cursor status
#define LCD_LN1         0x80    // DDRAM address of the 1st line
#define LCD_LN2         0xC0    // DDRAM address of the 2nd line

#define LCD_CG_SET      0x40    // Sets CGRAM address -> 0 0 1 add add add add add
#define LCD_DD_SET      0x80    // Sets DDRAM address -> 1 add add add add add add add


#define LCDNCHARS       16        // Define number of chars per line

//@todo check characters per line

//################### FUNCTION PROTOTYPES ###################

void lcd_init(void);                        // LCD initialization
void lcd_send_cmd(unsigned char data);      // Writes instruction to LCD
void lcd_send_dat(unsigned char data);      // Writes data to LCD
void pulseEnable(void);                     // Strobe the enable pin

void lcd_send_Uchar(unsigned char atemp);   // Dedicated routine for temperature display

#endif // SHIFT_LCD