/***********************************
 * File:    USART.c
 * Author:  zakkos
 ***********************************/

/* This is a dedicated minimal usart library with fixed values */

#include "USART.h"

//################### INITIALIZATION ROUTINE ###################

void usartInit(){
    TRISBbits.TRISB2 = 0;
    SPBRG = 25;
    TXSTA = 0b00100110;
    RCSTA = 0b10000000;
    return;
}

// Single character send

void usartSend(unsigned char c) {
    while (!PIR1bits.TXIF && !TXSTAbits.TRMT);
    TXREG = c;
    return;
}

// 16 bit send

void usartSend_16(unsigned int c) {
    while (!PIR1bits.TXIF && !TXSTAbits.TRMT);
    TXREG = c >> 8;
    while (!PIR1bits.TXIF && !TXSTAbits.TRMT);
    TXREG = c;
    return;
}

// String handler

void usartString(const char *msg) {
    while (*msg) {
        usartSend(*msg++);
    }
    return;
}

// EOL append

void usartEol() {
    while (!PIR1bits.TXIF && !TXSTAbits.TRMT);
    TXREG = 0x0D;
    while (!PIR1bits.TXIF && !TXSTAbits.TRMT);
    TXREG = 0x0A;
    return;
}

// Convert a signed int value to ascii and send the proper characters

void usartSigNum(signed int valore){
    char temp[5];
    usartString(itoa(temp, valore, 10));
    return;
}

// Convert an unsigned int value to ascii and send the proper characters

void usartUSigNum(unsigned int valore){
    char temp[5];
    usartString(utoa(temp, valore, 10));
    return;
}