/***********************************
 * File:    capture.c
 * Author:  zakkos
 ***********************************/

#include "capture.h"

//################### TURN ON MODULE ###################
void CCPOn(CaptureMode mode) {
    CCP1CON = 0x00;
    T1CON = 0x00;
    TMR1 = 0x0000;
    T1CON = 0x01;
    CCP1CON = mode;
    return;
}

//################### TURN OFF MODULE ###################
void CCPPOff() {
    CCP1CON = 0x00;
    return;
}

//################### INITIALIZE MODULE ###################
void CCPInit() {
    PIR1bits.CCP1IF = 0;
    PIR1bits.TMR1IF = 0;
    TRISBbits.TRISB3 = 1;   // CCP1 input pin 
    return;
}

//################### READ FROM MODULE ###################
    // Read from module and store value to oscCapture, value can then be processed at any time
void CCPRead() {
    static unsigned int CCPOld;
    unsigned int CCPNew;
    TMR0 = 0x00;
    CCPNew = CCPR1;
    PIR1bits.CCP1IF = 0x00;
    if (CCP_overflowCounter > 1) {
        oscCapture = 0xFFFE;
    } else {
        oscCapture = CCPNew - CCPOld;
    }
    CCP_overflowCounter = 0x00;
    CCPOld = CCPNew;
    return;
}