/***********************************
 * File:    qencoder.c
 * Author:  zakkos
 ***********************************/

#include "qencoder.h"

unsigned char _readEncoder(void) {
    unsigned char _newenc;
    static unsigned char _oldenc;
 
    // Change mask generation
    STATUSbits.C = 0x00;
    _newenc = ((_ENCX << 1) | _ENCY) & 0b00000011;
    unsigned char k = _newenc | (_oldenc << 2);
 
    // Change mask check
    #ifdef _doublestep
        switch (k) {
            case 0b0000:
            case 0b0101:
            case 0b1010:

            case 0b1011:
            case 0b0111:

            case 0b1111:
            case 0b0100:
            case 0b0010:
            case 0b0001:
            case 0b1000: break; //..nothing changed

            case 0b1110: _encpos++;
                break;

            case 0b1101: _encpos--;
                break;

    #elif	_fullstep
        switch (k) {
            case 0b0000:
            case 0b0101:
            case 0b1010:
            case 0b1111: break; //..nothing changed

            case 0b0001:
            case 0b0111:
            case 0b1110:
            case 0b1000: _encpos++;
                break;

            case 0b0100:
            case 0b0010:
            case 0b1011:
            case 0b1101: _encpos--;
                break;
    #else
        switch (k) {
            case 0b0000:
            case 0b0101:
            case 0b1010:
            case 0b1110:
            case 0b0001:
            case 0b0010:
            case 0b1101:
            case 0b1111: break; //..nothing changed

            case 0b0111:
            case 0b1000: _encpos++;
                break;

            case 0b0100:
            case 0b1011: _encpos--;
                break;
    #endif
    default: //..more than one bit changed
        break;
    }

    // Update encoder position variable
        _oldenc = _newenc;

    // Rollover behaviour check
    #ifdef	_rollover
        if (_encpos > _encMax && _encpos < 0xFF) {
            _encpos = 0x00;
        } else if (_encpos == 0xFF) {
            _encpos = _encMax;
        }
    #else
        if (_encpos >= _encMax && _encpos < 0xFF) {
            _encpos = _encMax;
        } else if (_encpos == 0xFF || _encpos == 0) {
            _encpos = 0x00;
        }
    #endif

        return _encpos;
}