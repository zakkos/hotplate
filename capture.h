/***********************************
 * File:    capture.h
 * Author:  zakkos
 ***********************************/

#ifndef CAPTURE_H
#define	CAPTURE_H

#ifdef	__cplusplus
extern "C" {
#endif

//################### VARIABLES ###################
    unsigned int oscCapture;
    //  Enumerator for capture mode
    typedef enum {
        EveryFallingEdge = 0x04, EveryRisingEdge, Every4RisingEdges, Every16RisingEdges
    } CaptureMode;

//################### FUNCTIONS PROTOTYPES ###################
    void CCPOn(CaptureMode mode);
    void CCPPOff();
    void CCPInit();
    void CCPRead();

#ifdef	__cplusplus
}
#endif

#endif	/* CAPTURE_H */

