/***********************************
 * File:    hotplate.c
 * Author:  zakkos
 *
 * Device:  PIC16F628A
 ***********************************/

//################### CONFIG ###################

#pragma config FOSC = INTOSCIO  // Oscillator Selection bits (INTRC oscillator: I/O function on RA6/OSC2/CLKOUT pin, I/O function on RA7/OSC1/CLKIN)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = ON       // Power-up Timer Enable bit (PWRT enabled)
#pragma config MCLRE = OFF      // RA5/MCLR pin function select (RA5/MCLR pin function is digital input, MCLR internally tied to VDD)
#pragma config BOREN = ON      // Brown-out Reset Enable bit (BOD Reset disabled)
#pragma config LVP = OFF        // Low-Voltage Programming Enable bit (RB4/PGM pin has digital I/O function, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data Code Protection bit (Data memory code protection off)
#pragma config CP = OFF         // Code Protection bits (Program memory code protection off)

//################### DEFINITIONS ###################

#define _XTAL_FREQ 4000000      // Clock speed
#define PWM_OUT PORTAbits.RA2   // PWM output pin
    // PID TERMS
#define PropGain_Tempr 2
#define PropGain_TemprDiv 1
#define IntegGain_Tempr 1
#define IntegGain_TemprDiv 1
#define IntegLimit 3500         // That's equivalent to percent * 1000 (1500 = 1.5%)
#define DerivGain_Tempr 2
#define DerivGain_TemprDiv 1
#define DerivDelay 200          // Expressed in tens of milliseconds (n * 10ms, or repetitions of the 100Hz mains interrupt)
    // FINITE STATES INDEX FOR MENU PAGES
    // 0 = Main page, 1 = Temperature set page, 2 = Set action, 3 = Error display and safe halt
#define  i_STATE_1    0
#define  i_STATE_2    1
#define  i_STATE_3    2
#define  i_STATE_4    3

// #define DEBUG_OUT               // If defined (uncommented) data will be presented at the serial out port

//################### GLOBAL VARIABLES ###################

unsigned char pwm_Cursor;
unsigned char pwm_Duty;
unsigned char debounce_Counter = 0x0A;
unsigned char CCP_overflowCounter;

struct {
    unsigned CCPF : 1;          // A capture has occurred
    unsigned SerTX : 1;         // Transmission delay has elapsed
    unsigned BTN : 1;           // Button is pressed
    unsigned LCD_Update :1;     // LCD update delay has elapsed
    unsigned probe_Fail :1;     // Unable to find probe
} flags;

//################### INCLUSIONS ###################

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
    // LUTs
#include "temp_lut.h"           // Calculated temperatures LUT 
    // external libs
#include "shift_lcd.c"
#include "qencoder.c"
#include "capture.c"

#ifdef DEBUG_OUT
    #include "USART.c"
#endif

//################### TEST OPTIMIZATION MODE ###################

#if _HTC_EDITION_ == 2
    #warning Operating in Pro mode
#endif

#if _HTC_EDITION_ == 1
    #warning Operating in Standard mode
#endif

#if _HTC_EDITION_ == 0
    #warning Operating in Free mode
#endif

//################### FUNCTIONS PROTOTYPES ###################

void interrupt ISR_Routine(void);
unsigned char get_temp(unsigned int osc_Value);

//################### MAIN FUNCTION ###################

void main() {
    /* LOCAL VARIABLES DEFINITIONS */
    unsigned char actual_Temperature = 0x00;
    signed char temperature_Error = 0x00;
    signed char old_Error = 0x1E;
    signed int PropComp_Tempr = 0x0000;
    signed int IntegComp_Tempr = 0x0000;
    signed char DerivComp_Tempr = 0x00;
    signed int PID_Tempr = 0x00;
    unsigned char ind_FSM = 0;
    unsigned char debug_delay = 10;
    unsigned char derivative_delay = 0x00;

    /* REGISTERS INITIALIZATION */
    OPTION_REG = 0b11010111;    //@todo Explicitate, remove duplicates
    PIE1 = 0x00;
    PIR1 = 0x00;
    T1CON = 0x00;
    
    /* PORTS INITIALIZATION */
        //PRELOAD
    PORTA = 0x00;
    PORTB = 0x00;
        //Disable comparators @todo make more meaningful setting
    CMCON = 0x07;
        //TRISTATE SETTING
    TRISA = 0b11100011;
    TRISB = 0b11001011;

    /* Timer0 INITIALIZATION */
    OPTION_REGbits.T0CS = 0;    // Set clock source (internal)
    OPTION_REGbits.PSA = 0;     // Set prescaler assign (timer0)
    OPTION_REGbits.PS = 0b111;  // Set prescaler to 1:256

    __delay_ms(100);    // Small startup delay

    /* INITIALIZATION ROUTINES */
    lcd_init();
        // SPLASH SCREEN
    lcd_home();
    writeString(" WARNING");
    lcd_send_cmd(LCD_LN2);
    writeString("! HOT!");
    __delay_ms(1500);

        // Enable interrupts
    INTCONbits.GIE = 1;
    INTCONbits.PEIE = 1;

#ifdef DEBUG_OUT
    usartInit();
#endif

        // CCP module setup (CCP module is used for measuring temperature through frequency)
    CCPInit();
    CCPOn(Every4RisingEdges);
        // Timer0 interrupt setup (Timer0 is used for probe detection)
    TMR0 = 0;
    INTCONbits.T0IF = 0;
    INTCONbits.T0IE = 1;
        // Wait until first valid CCP reading, if timeout occurs then get into safe mode (raise probe fail flag)
    while (!PIR1bits.CCP1IF) {
        INTCONbits.INTE = 0;
        if (flags.probe_Fail){
            break;
        }
    }
        // Enable RB0 external interrupt
    INTCONbits.INTE = 1;
    CCPRead();
        // Recall last temperature setting
    unsigned char set_temp = eeprom_read(0x00);
    if (set_temp > 150) set_temp = 40;
        // Set displayed temperature to set temperature
    unsigned char disp_temp = set_temp;
        // Enable CCP module interrupt
    PIE1bits.CCP1IE = 1;

    /***********************************
     * MAIN LOOP
     ***********************************/
    for (;;) {
            // Probe timeout check
        if (flags.probe_Fail){
            ind_FSM = i_STATE_4;
        }
            // Get actual temperature and calculate error for PID 
        actual_Temperature = get_temp(oscCapture);
        temperature_Error = set_temp - actual_Temperature;
        if (temperature_Error > 30) PID_Tempr = 100;
        else if (temperature_Error <= 30 && flags.CCPF) {
            flags.CCPF = 0;
            // Calculate Proportional component of PID
            PropComp_Tempr = temperature_Error * PropGain_Tempr;

            // Calculate Integral component of PID
            IntegComp_Tempr += (temperature_Error * IntegGain_Tempr);

            // Limit Integral to its limit value (plus or minus zero)
            if (IntegComp_Tempr > IntegLimit) {
                IntegComp_Tempr = IntegLimit;
            } else if (IntegComp_Tempr < -IntegLimit) {
                IntegComp_Tempr = -IntegLimit;
            }
            // Calculate Derivative component of PID
            // A delay ensures the error difference might be more than 0, 
            // this also regulates for how much time the derivative term is applied to the equation
            if (!derivative_delay) {
            DerivComp_Tempr = (temperature_Error - old_Error) * DerivGain_Tempr;
            old_Error = temperature_Error;
            derivative_delay = DerivDelay;
            }
            derivative_delay--;
            // PID computation:
            PID_Tempr = (PropComp_Tempr + (IntegComp_Tempr / 1000) + DerivComp_Tempr);
        }
            // Keep PWM duty cycle between 0 and 100
        if (PID_Tempr > 100) {
            pwm_Duty = 100;
        } else if (PID_Tempr < 0)pwm_Duty = 0;
        else pwm_Duty = PID_Tempr;

        /***********************************
         * SERIAL DEBUG TRANSMIT
         ***********************************/

#ifdef	DEBUG_OUT
        if (flags.SerTX) {
            flags.SerTX = 0;
            debug_delay--;
            if (!debug_delay) {
               usartUSigNum(actual_Temperature);
                usartSend(0x2C);
                usartSigNum(temperature_Error);
                usartSend(0x2C);
                usartSigNum(PropComp_Tempr);
                usartSend(0x2C);
                usartSigNum(IntegComp_Tempr);
                usartSend(0x2C);
                usartSigNum(DerivComp_Tempr);
                usartSend(0x2C);
                usartSigNum(PID_Tempr);
                usartSend(0x2C);
                usartUSigNum(pwm_Duty);
                usartEol();
                debug_delay = 10;
            }
        }
#endif
        /***********************************
         * FINITE STATE MACHINE (DISPLAY AND COMMANDS)
         ***********************************/
        switch (ind_FSM) {
                // Main page
            case i_STATE_1:
                if (flags.LCD_Update) {
                    flags.LCD_Update = 0;
                    lcd_send_cmd(LCD_CLR);
                    lcd_home();
                    lcd_send_Uchar(actual_Temperature);
                    lcd_send_dat(0xDF); // ° Character
                    lcd_send_cmd(0xC4); // Second line alignment
                    lcd_send_Uchar(set_temp);
                    lcd_send_dat(0xDF);
                }
                    // If button is pressed and debounced then change state
                if (!debounce_Counter && flags.BTN) {
                    ind_FSM++;
                    flags.BTN = 0;
                }
                break;
                // Temperature set page
            case i_STATE_2:
                if  (flags.LCD_Update){
                    flags.LCD_Update = 0;
                    _encpos = (disp_temp - 40);
                    disp_temp = _readEncoder() + 40;
                    lcd_send_cmd(LCD_CLR);
                    lcd_home();
                    writeString("SET TEMP");
                    lcd_send_cmd(0xC4);
                    lcd_send_Uchar(disp_temp);
                    lcd_send_dat(0xDF);
                }
                if (!debounce_Counter && flags.BTN) {
                    ind_FSM++;
                    flags.BTN = 0;
                }
                break;
                // Set action
            case i_STATE_3:
                ind_FSM = i_STATE_1;
                eeprom_write(0x00, disp_temp);
                set_temp = disp_temp;
                break;
                // Error display and safe halt
            case i_STATE_4:
                    // Disable all interrupts and set output to 0
                INTCONbits.GIE = 0;
                INTCONbits.PEIE = 0;
                PWM_OUT = 0;
                    // Disaplay error message
                lcd_send_cmd(LCD_CLR);
                lcd_home();
                writeString("   ERROR");
                lcd_send_cmd(LCD_LN2);
                writeString("  404");   // Probe not found ;)
                SLEEP();

            default:
                ind_FSM = i_STATE_1;
                break;
        }

    }
}

/***********************************
 * INTERRUPT ROUTINE
 ***********************************/
void interrupt ISR_Routine(void) {

    //################### TIMER1 OVERFLOW ###################

    if (PIR1bits.TMR1IF) {
        CCP_overflowCounter++;
        PIR1bits.TMR1IF = 0;
    }

    //################### CCP MODULE READ SOMETHING###################

    if (PIR1bits.CCP1IF) {
        CCPRead();
    }

    //################### MAINS INTERRUPT ###################

    if (INTCONbits.INTF) {
        if (pwm_Duty && pwm_Duty >= pwm_Cursor) {
            PWM_OUT = 1;
        } else {
            PWM_OUT = 0;
        }
        pwm_Cursor++;
        if (pwm_Cursor > 100) pwm_Cursor = 0;
        INTCONbits.INTF = 0;
        flags.CCPF = 1;
        flags.LCD_Update = 1;
        flags.SerTX = 1;
    }

    //################### BUTTON DEBOUNCE ###################

    if (!PORTAbits.RA5 && debounce_Counter) {
        debounce_Counter--;
        flags.BTN = 1;
    } else if (PORTAbits.RA5) {
        debounce_Counter = 0x0A;
        flags.BTN = 0;
    }

    //################### TIMER0 OVERFLOW ###################

    if (INTCONbits.T0IF){
        flags.probe_Fail = 1;
        INTCONbits.T0IF = 0;
    }

    return;
}

/***********************************
 * TEMPERATURE LUT RETRIEVAL
 ***********************************/
unsigned char get_temp(unsigned int osc_Value) {
    osc_Value /= 4;
        // Compare oscillator value against LUT items and retrieve proper index
    for (unsigned char i = 155; i; i--) {
        if (osc_Value < temperature[i]) return i + 5;
    }
        // Default return value
    return 5;
}
