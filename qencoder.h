/***********************************
 * File:    qencoder.h
 * Author:  zakkos
 ***********************************/

#ifndef QENCODER_H
#define	QENCODER_H

#ifdef	__cplusplus
extern "C" {
#endif

//################### DEFINITIONS ###################

    // if _rollover is defined when _encMax is reached the count goes to 0, else it stops at _encMax
//#define _rollover
 
    // if _fullstep is defined, every valid gray code change is translated into an increase or decrease of the counter
    // if _doublestep is defined, every two half step (from 11 back to 11 through 10 00 01) counts as one increase (or decrease) of the counter
    // else the counter updates only when there is a transition into a full 11 or 00
//#define _fullstep
#define _doublestep

    //max counter position
#define _encMax 110

    //ports
#define _ENCX PORTAbits.RA1
#define _ENCY PORTAbits.RA0

//################### VARIABLES ###################
    
unsigned char _encpos;
    
//################### FUNCTIONS PROTOTYPES ###################

unsigned char _readEncoder(void);

#ifdef	__cplusplus
}
#endif

#endif	/* QENCODER_H */

