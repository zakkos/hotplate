/***********************************
 * File:    shift_74ls164.c
 * Author:  zakkos
 ***********************************/

#include "shift_74ls164.h"

// Shift out a char

void _shiftOut(volatile unsigned char value) {
#ifdef	SHIFT_INVERSE
    value = ~value;
#endif
    STATUSbits.C = 0;
    for (unsigned char i = 8; i; i--) {
        value = value << 1;
        SHIFT_OUT = STATUSbits.C;
        __delay_us(1);
        SHIFT_CLK = 1;
        __delay_us(1);
        SHIFT_CLK = 0;
    }
    return;
}