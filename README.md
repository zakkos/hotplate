# README #

### Help with Code review  ###

* I published this code and schematics to allow for peer review and become a better programmer

### What is this? ###

* I was in need of a hotplate to bathe 3d prints in acetone vapours so i thought of building one. I had a PIC16F628A lying around so i decided to use what I had around and build the plate and write the code to control it.

### Why? ###

* The code compiles and works beautifully so i'm quite happy with it but I had some problems with compiler optimization (XC8 1.34 in PRO mode) and the "volatile" qualifier, so I decided to get help from the community (that means you!)
* I noticed that without peer reviews and code reviews I wouldn't get any better. So I'm asking for feedbacks on my "homeworks".

### Peculiarity ###

* Since the PIC16F628A doesn't have an ADC I explored different ways to read a thermistor. I ended up designing an oscillator that changes its frequency according to the thermistor resistance value. It works! Better than i expected actually, but this might not be the best way to do it, anyway that's not a problem I just wanted to point out a feature that may be confusing to most people!

### THANK YOU!

* I'd really appreciate some feedback on my work

* you can PM me on the microchip forum 
-> http://www.microchip.com/forums/

* or answer the thread I opened for this project
-> http://www.microchip.com/forums/m878150.aspx

* ZaKKoS