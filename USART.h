/***********************************
 * File:    USART.h
 * Author:  zakkos
 ***********************************/

#ifndef USART_H
#define	USART_H

#ifdef	__cplusplus
extern "C" {
#endif

//################### FUNCTIONS PROTOTYPES ###################

    void usartInit();
    void usartSend(unsigned char c);
    void usartSend_16(unsigned int c);
    void usartString(const char *msg);
    void usartSigNum(signed int valore);
    void usartUSigNum(unsigned int valore);
    void usartEol();
    
#ifdef	__cplusplus
}
#endif

#endif	/* USART_H */

