/***********************************
 * File:    shift_lcd.c
 * Author:  zakkos
 ***********************************/

#include "shift_lcd.h"

//################### INITIALIZATION ROUTINE ###################

void lcd_init() {
    _shiftOut(0x00);    // Clear the shift register
    CMD_REG;
    __delay_ms(300);

/*    //*** setting LCD with 8bit
    _shiftOut(0x30);
    pulseEnable();
    __delay_ms(5);
    _shiftOut(0x30);                        //FIXME: For now initialization relies on internal LCD reset
    pulseEnable();
    __delay_ms(2);
    _shiftOut(0x30);
    pulseEnable();
    __delay_ms(1);*/

    lcd_send_cmd(LCD_8b_2L); //Function set
    lcd_send_cmd(LCD_DISPON); //Display on
    lcd_send_cmd(LCD_INC); //entry mode
    lcd_send_cmd(LCD_CLR); //Display clear
    return;
}

// Pulse the enable pin loading data into LCD own register

void pulseEnable() {
    DispE = 0;
    __delay_us(2);
    DispE = 1;
    __delay_us(2);
    DispE = 0;
    __delay_us(2);
    return;
}

// Send data

void lcd_send_dat(unsigned char c) {
    DispE = 0;
    DATA_REG;
    __delay_us(60);
    _shiftOut(c);
    pulseEnable();
    return;
}

// Send command

void lcd_send_cmd(unsigned char c) {
    DispE = 0;
    CMD_REG;
    __delay_us(60);
    _shiftOut(c);
    pulseEnable();
    return;
}

// String handler

void writeString(const char *msg) {
    while (*msg) {
        lcd_send_dat(*msg++);
    }
    return;
}

// Set lcd cursor at HOME position (needs special handling)

void lcd_home(void) {
    DispE = 0;
    CMD_REG;
    __delay_us(60);
    _shiftOut(LCD_HOME);
    pulseEnable();
    __delay_ms(2);
    return;
}

// Dedicated routine for our temperature display (works only in range 0 - 199)

void lcd_send_Uchar(unsigned char atemp) {
    unsigned char temp = atemp;
    if (temp >= 100) {
        temp -= 100;
        lcd_send_dat('1');
    } else {
        lcd_send_dat(0x20);
    }
    if (atemp >= 10) {
        lcd_send_dat('0' + (temp / 10));
    } else {
        lcd_send_dat(0x20);
    }
    lcd_send_dat('0' + (temp % 10));
    return;
}